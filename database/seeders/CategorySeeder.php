<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    public function run()
    {
        DB::table('categories')->insert([
            [
                'id' => 1,
                'parent_id' => 0,
                'name' => 'Mobile',
            ], [
                'id' => 2,
                'parent_id' => 1,
                'name' => 'Samsung',
            ], [
                'id' => 3,
                'parent_id' => 1,
                'name' => 'MI',
            ], [
                'id' => 4,
                'parent_id' => 1,
                'name' => 'Oppo',
            ], [
                'id' => 5,
                'parent_id' => 1,
                'name' => 'Vivo',
            ], [
                'id' => 6,
                'parent_id' => 0,
                'name' => 'Laptop',
            ], [
                'id' => 7,
                'parent_id' => 6,
                'name' => 'Dell',
            ], [
                'id' => 8,
                'parent_id' => 6,
                'name' => 'HP',
            ], [
                'id' => 9,
                'parent_id' => 6,
                'name' => 'Lenovo',
            ], [
                'id' => 10,
                'parent_id' => 6,
                'name' => 'ASUS',
            ], [
                'id' => 11,
                'parent_id' => 0,
                'name' => 'Tablate',
            ], [
                'id' => 12,
                'parent_id' => 11,
                'name' => 'Apple',
            ], [
                'id' => 13,
                'parent_id' => 11,
                'name' => 'Samsung',
            ], [
                'id' => 14,
                'parent_id' => 11,
                'name' => 'Motorolla',
            ],
        ]);
    }
}

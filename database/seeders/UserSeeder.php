<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([
            [
                'id' => 1,
                'name' => 'Admin',
                'email' => 'admin@admin.com',
                'role' => 'admin',
                'password' => bcrypt('password'),
            ], [
                'id' => 2,
                'name' => 'Hari',
                'email' => 'hari@test.com',
                'role' => 'user',
                'password' => bcrypt('123456'),
            ],
        ]);
    }
}

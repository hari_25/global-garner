function deleteRecord(num) {
    if (confirm(areYouSure)) {
        $('#frmDelete' + num).submit();
    }

    return false;
}

function imagePreview(input, img_id = '', img_err = '') {
    let input_file = input.files[0];

    if (input.files && input_file) {
        let fsize = input_file.size;
        let fname = input_file.name;
        let fextension = fname.split('.').pop();
        let validExtensions = ["jpg", "jpeg", "png", "bmp", "gif"];
        let reader = new FileReader();

        if ($.inArray(fextension, validExtensions) == -1) {
            $('#' + img_err).css('display', 'block').html('Only photo is allowed');
            input.value = "";
            $('#' + img_id).css('display', 'block').attr('src', '');
            return false;
        }

        if (fsize > 2097152) {
            $('#' + img_err).css('display', 'block').html('Photo should be less than 2mb');
            input.value = "";
            $('#' + img_id).css('display', 'block').attr('src', '');
            return false;
        }

        $('#' + img_err).css('display', 'none').html('');

        reader.onload = function (e) {
            $('#' + img_id).css('display', 'block').attr('src', e.target.result);
        }

        reader.readAsDataURL(input_file);

        return true;
    } else {
        $('#' + img_id).css('display', 'none').attr('src', '');
    }
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    let charCode = (evt.which) ? evt.which : evt.keyCode;

    if (charCode < 48 || charCode > 57) {
        return false;
    }

    return true;
}

function isFloat(evt) {
    evt = (evt) ? evt : window.event;
    let charCode = (evt.which) ? evt.which : evt.keyCode;

    if (charCode < 46 || charCode > 57) {
        return false;
    }

    if ((charCode == 46 && evt.target.value.split('.').length > 1) || charCode == 47) {
        return false;
    }

    return true;
}

function samePriceQTY(input, qty_id, size_length) {
    let price = $('#' + input).val();

    if (price) {
        for (let index = 1; index <= size_length; index++) {
            $(qty_id + index)
                .val(price)
                .prop('readonly', true);
        }
    } else {
        for (let index = 1; index <= size_length; index++) {
            $(qty_id + index)
                .prop('readonly', false);
        }
    }
}

function selectAllAttribute(cbID, optID) {
    if ($("#" + cbID).is(':checked')) {
        $("." + optID + " > option").prop("selected", "selected");
        $("." + optID).trigger("change");
    } else {
        $("." + optID).each(function () {
            $(this).val('').change();
        });
    }
}

$(function () {
    $(".select2").on('select2:unselecting', function (e) {
        $("#" + $(this).data("cb_id")).prop('checked', false);
    });

    $("#txtTag").keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();

            return false;
        }
    });

    $("#txtTag").on({
        focusout: function () {
            var txt = this.value.replace(/[^a-z0-9 \+\-\.\#]/ig, '');

            if (txt) $("<span/>", {
                text: txt.trim().toLowerCase(),
                insertBefore: this
            });

            $(this).val('').focus();
        },
        keyup: function (ev) {
            if (/(188|13)/.test(ev.which)) $(this).focusout();
        }
    });

    $('#tags').on('click', 'span', function () {
        if (confirm("Remove " + $(this).text() + "?")) $(this).remove();
    });
})
<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\FileUploadTrait;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Support\Facades\Gate;

class ProductController extends Controller
{
    use FileUploadTrait;

    public function index()
    {
        abort_if(!Gate::allows('product-list'), 401);

        $products = Product::latest()->get();

        return view('admin.product.index', compact('products'));
    }

    public function create()
    {
        abort_if(!Gate::allows('product-create'), 401);

        $categories = Category::orderBy('id', 'desc')->where('parent_id', 0)->get()->pluck('name', 'id')->prepend('Please Select', '');
        $sub_categories = ['' => 'Please Select'];

        return view('admin.product.create', compact('categories', 'sub_categories'));
    }

    public function store(Request $request)
    {
        abort_if(!Gate::allows('product-create'), 401);

        $request->validate([
            'name' => ['required', 'unique:products', 'max:200'],
            'price' => ['required'],
            'status' => ['required'],
            'image' => ['required'],
            'category_id' => ['required'],
            'sub_category_id' => ['required'],
            'description' => ['nullable'],
        ]);

        $productData = $this->imageUpload($request, [], 'products')->all();

        Product::create($productData);

        return redirect()->route('product.index')->with('success', 'Product saved successfully');
    }

    public function edit(Product $product)
    {
        abort_if(!Gate::allows('product-edit'), 401);

        $categories = Category::orderBy('id', 'desc')->where('parent_id', 0)->get()->pluck('name', 'id');
        $sub_categories = Category::where('parent_id', $product->category_id)->get()->pluck('name', 'id');

        return view('admin.product.edit', compact('product', 'categories', 'sub_categories'));
    }

    public function update(Request $request, Product $product)
    {
        abort_if(!Gate::allows('product-edit'), 401);

        $request->validate([
            'name' => ['required', 'unique:products,name,' . $product->id, 'max:200'],
            'price' => ['required'],
            'status' => ['required'],
            'image' => ['nullable'],
            'category_id' => ['required'],
            'sub_category_id' => ['required'],
            'description' => ['nullable'],
        ]);

        $productData = $this->imageUpload($request, $product->toArray(), 'products')->all();

        $product->update($productData);

        return redirect()->route('product.index')->with('success', 'Product updated successfully');
    }

    public function destroy(Product $product)
    {
        abort_if(!Gate::allows('product-delete'), 401);

        $product->delete();

        return back()->with('success', 'Product deleted successfully');
    }

    public function subCategory($id)
    {
        $sub_categories = Category::where('parent_id', $id)->get()->pluck('name', 'id')->prepend('Please Select', '');

        return response()->json([
            'success' => true,
            'msg' => 'Category Found',
            'html' => view('partials.sub-category', compact('sub_categories'))->render(),
        ]);
    }
}

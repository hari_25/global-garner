<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\User;

class DashboardController extends Controller
{
    public function index()
    {
        $product_count = Product::count();
        $user_count = User::count();

        return view('admin.dashboard', compact('product_count', 'user_count'));
    }
}

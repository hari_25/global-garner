<?php

namespace App\Http\Controllers\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

trait FileUploadTrait
{
    // imageUpload function is used for uploading single & multiple image
    // input_name is used for multiple image upload

    public function imageUpload(Request $request, $model = [], $folder_name, $input_name = '', $width = 400, $height = 400)
    {
        $image_url = "public/$folder_name";
        $thumb_path = "public/$folder_name/thumb";
        $new_request = $request;

        foreach ($request->all() as $key => $value) {
            if ($key == $input_name) {
                $image_arr = [];

                foreach ($request->$input_name as $arr_key => $arr_value) {
                    $file_name = md5(microtime()) . '.' . $arr_value->extension();

                    // Image store with actual size
                    $arr_value->storeAs($image_url, $file_name);

                    // Image store with (160 x 120) size
                    $imagePath = $arr_value->storeAs($thumb_path, $file_name);
                    $image = Image::make($arr_value)->resize($width, $height)->encode();
                    Storage::put($imagePath, $image);
                    $image_arr[] = $file_name;
                }

                $new_request = new Request(array_merge($new_request->all(), [$key => $image_arr]));
            } else if ($request->hasFile($key)) {
                if ($request->method() == 'PUT') {
                    $this->deleteOldImage($folder_name, $model['image']->url, $model['image']->thumb);
                }

                $file_name = md5(microtime()) . '.' . $request->file($key)->extension();

                // Image store with actual size
                $request->file($key)->storeAs($image_url, $file_name);

                // Image store with default size
                $imagePath = $request->file($key)->storeAs($thumb_path, $file_name);
                $image = Image::make($request->file($key))->resize($width, $height)->encode();
                Storage::put($imagePath, $image);

                $new_request = new Request(array_merge($new_request->all(), [$key => $file_name]));
            }
        }

        return $new_request;
    }

    public function deleteOldImage($folder_name, $image_url, $image_thumb)
    {
        $path = public_path() . '/storage/' . $folder_name . '/';
        $thumb = public_path() . '/storage/' . $folder_name . '/thumb/';

        $image_url = explode('/', $image_url);
        $image_url = end($image_url);
        $image_url = $path . $image_url;
        $image_thumb = explode('/', $image_thumb);
        $image_thumb = end($image_thumb);
        $image_thumb = $thumb . $image_thumb;

        if (File::exists($image_url)) {
            unlink($image_url);
        }

        if (File::exists($image_thumb)) {
            unlink($image_thumb);
        }
    }
}

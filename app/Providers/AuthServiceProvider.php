<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        $user = Auth::user();

        Gate::define('product-list', function ($user) {
            return in_array($user->role, ['admin', 'user']);
        });

        Gate::define('product-create', function ($user) {
            return $user->role == 'user';
        });

        Gate::define('product-edit', function ($user) {
            return $user->role == 'user';
        });

        Gate::define('product-delete', function ($user) {
            return $user->role == 'user';
        });
    }
}

@extends('layouts.admin')

@section('content')
    <div class="card">
        <div class="card-header h4">
            Products List

            @can('product-create')
                <a class="btn btn-success float-end" href="{{ route('product.create') }}">
                    Add Product
                </a>
            @endcan
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover datatable">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Name
                            </th>
                            <th>
                                Category
                            </th>
                            <th>
                                Sub Category
                            </th>
                            <th>
                                Image
                            </th>
                            <th>
                                Price
                            </th>
                            <th>
                                status
                            </th>
                            @canany(['product-update', 'product-delete'])
                                <th class="action">
                                    Actions
                                </th>
                            @endcanany
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($products as $key => $item)
                            <tr>
                                <td>
                                    {{ $key + 1 }}
                                </td>
                                <td>
                                    {{ $item->name }}
                                </td>
                                <td>
                                    {{ $item->category->name }}
                                </td>
                                <td>
                                    {{ $item->subCategory->name }}
                                </td>
                                <td class="text-center">
                                    <img src="{{ $item->image->url }}" alt="{{ $item->name }}" width="120" height="80"
                                        class="rounded">
                                </td>
                                <td>
                                    {{ $item->price }}
                                </td>
                                <td>
                                    @if ($item->status)
                                        <span class="badge bg-success">Active</span>
                                    @else
                                        <span class="badge bg-danger">Inactive</span>
                                    @endif
                                </td>
                                @canany(['product-update', 'product-delete'])
                                    <td>
                                        @can('product-edit')
                                            <a href="{{ route('product.edit', [$item->id]) }}"
                                                class="my-2 btn-sm btn btn-warning">
                                                Edit
                                            </a>
                                        @endcan
                                        @can('product-delete')
                                            <form action="{{ route('product.destroy', $item->id) }}" method="POST"
                                                onsubmit="return confirm('Are you sure, want to delete?');"
                                                style="display: inline-block;">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <button type="submit" class="my-2 btn-sm btn btn-danger">
                                                    Delete
                                                </button>
                                            </form>
                                        @endcan
                                    </td>
                                @endcanany
                            </tr>
                        @empty
                            <tr>
                                <td colspan="10" class="text-center">
                                    No recored found
                                </td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('.datatable').DataTable({
                "pageLength": 50
            });
        });
    </script>
@endsection

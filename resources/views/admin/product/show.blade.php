@extends('layouts.admin')

@section('content')
    <div class="card">
        <div class="card-header">
            {{ trans('global.show') }} Service

            <a class="btn btn-secondary float-right" href="{{ route('admin.service.index') }}">
                {{ trans('global.back') }}
            </a>
        </div>

        <div class="card-body">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            Title
                        </th>
                        <td>
                            {{ $service->title }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Slug
                        </th>
                        <td>
                            {{ $service->slug }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Price
                        </th>
                        <td>
                            {{ $service->price }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Image
                        </th>
                        <td>
                            <img src="{{ $service->image_url }}" alt="{{ $service->title }}" width="200" height="120">
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Therapy
                        </th>
                        <td>
                            @forelse ($service->therapies as $row)
                                <span class="badge badge-pill badge-primary h6 mr-2 px-3 py-2 text-center">
                                    {{ $row->therapy->title }}
                                </span>
                            @empty
                            @endforelse
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Description
                        </th>
                        <td>
                            {!! $service->description !!}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@extends('layouts.admin')
@section('content')

    <div class="card">
        <div class="card-header">
            Edit Prodict

            <a class="btn btn-secondary float-end" href="{{ route('product.index') }}">
                Back
            </a>
        </div>

        <div class="card-body">
            {!! Form::model($product, ['method' => 'PUT', 'route' => ['product.update', $product->id], 'autocomplete' => 'off', 'files' => true]) !!}
            <div class="row">
                <div class="form-group col-6">
                    <label class="required">
                        Name
                    </label>
                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                </div>

                <div class="form-group col-6">
                    <label>
                        Category
                    </label>
                    <select class="form-control" name="category_id" id="optCategory">
                        @foreach ($categories as $key => $category)
                            <option value="{{ $key }}" {{ $key == $product->category->id ? 'selected' : '' }}>
                                {{ $category }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-6" id="subCategory">
                    <label>
                        Category
                    </label>
                    <select class="form-control" name="sub_category_id">
                        @foreach ($sub_categories as $key => $category)
                            <option value="{{ $key }}" {{ $key == $product->subCategory->id ? 'selected' : '' }}>
                                {{ $category }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-6">
                    <label class="required">
                        Price
                    </label>
                    {!! Form::text('price', old('price'), ['class' => 'form-control', 'maxlength' => '10', 'onkeypress' => 'return isFloat(event)', 'required' => '']) !!}
                </div>

                <div class="form-group col-6">
                    @include('partials.single-image-upload', [
                    'input_name' => 'image',
                    'lable_name' => 'Image',
                    'image_view_name' => 'image_view',
                    'image_error_name' => 'image_error',
                    'required' => '',
                    'image_url' => $product->image->url
                    ])
                </div>

                <div class="form-group col-6">
                    <label class="required">
                        Status
                    </label>
                    <div class="form-check">
                        {!! Form::radio('status', old('status', '1'), false, ['class' => 'form-check-input', 'required' => '', 'id' => 'rbActive']) !!}
                        <label class="required" for="rbActive">
                            Active
                        </label>
                    </div>
                    <div class="form-check">
                        {!! Form::radio('status', old('status', '0'), false, ['class' => 'form-check-input', 'required' => '', 'id' => 'rbInactive']) !!}
                        <label class="required" for="rbInactive">
                            Inactive
                        </label>
                    </div>
                </div>

                <div class="form-group col-12">
                    <label class="required">
                        Description
                    </label>
                    {!! Form::textarea('description', old('description', $product->description), ['class' => 'form-control ckeditor', 'placeholder' => '']) !!}
                </div>

                <div class="form-group text-right col-12 mt-4">
                    <button class="btn btn-warning float-end" type="submit">
                        Update
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('#optCategory').on('change', function() {
            $.ajax({
                type: 'get',
                url: "{{ url('sub_category') }}/" + this.value,
            }).done(function(res) {
                $('.preloader').css('height', '0px');

                if (res.success) {
                    $('#subCategory').empty().html(res.html);

                    toastr.success(res.msg);
                } else {
                    toastr.error(res.msg);
                }
            });
        });
    </script>
@endsection

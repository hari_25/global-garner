@extends('layouts.admin')
@section('content')

    <div class="card">
        <div class="card-header h4">
            Create Product
        </div>

        <div class="card-body">
            {!! Form::open(['method' => 'POST', 'route' => ['product.store'], 'autocomplete' => 'off', 'files' => true]) !!}
            <div class="row">
                <div class="form-group col-6">
                    <label class="required">
                        Name
                    </label>
                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                </div>

                <div class="form-group col-6">
                    <label>
                        Category
                    </label>
                    {!! Form::select('category_id', $categories, old('category_id'), ['class' => 'form-control', 'required' => '', 'id' => 'optCategory']) !!}
                </div>

                <div class="form-group col-6" id="subCategory">
                    @include('partials.sub-category', ['sub_categories' => $sub_categories])
                </div>

                <div class="form-group col-6">
                    <label class="required">
                        Price
                    </label>
                    {!! Form::text('price', old('price'), ['class' => 'form-control', 'maxlength' => '10', 'onkeypress' => 'return isFloat(event)', 'required' => '']) !!}
                </div>

                <div class="form-group col-6">
                    @include('partials.single-image-upload', [
                    'input_name' => 'image',
                    'lable_name' => 'Image',
                    'image_view_name' => 'image_view',
                    'image_error_name' => 'image_error',
                    'required' => 'required',
                    'image_url' => ''
                    ])
                </div>

                <div class="form-group col-6">
                    <label class="required">
                        Status
                    </label>
                    <div class="form-check">
                        {!! Form::radio('status', old('status', 1), true, ['class' => 'form-check-input', 'required' => '', 'id' => 'rbActive']) !!}
                        <label class="required" for="rbActive">
                            Active
                        </label>
                    </div>
                    <div class="form-check">
                        {!! Form::radio('status', old('status', 0), false, ['class' => 'form-check-input', 'required' => '', 'id' => 'rbInactive']) !!}
                        <label class="required" for="rbInactive">
                            Inactive
                        </label>
                    </div>
                </div>

                <div class="form-group col-12">
                    <label class="required">
                        Description
                    </label>
                    {!! Form::textarea('description', old('description'), ['class' => 'form-control ckeditor', 'placeholder' => '']) !!}
                </div>

                <div class="form-group text-right col-12 mt-4">
                    <button class="btn btn-success float-end" type="submit">
                        Save
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('#optCategory').on('change', function() {
            $.ajax({
                type: 'get',
                url: "{{ url('sub_category') }}/" + this.value,
            }).done(function(res) {
                $('.preloader').css('height', '0px');

                if (res.success) {
                    $('#subCategory').empty().html(res.html);

                    toastr.success(res.msg);
                } else {
                    toastr.error(res.msg);
                }
            });
        });
    </script>
@endsection

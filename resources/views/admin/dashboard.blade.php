@extends('layouts.admin')

@section('content')
    <div class="row">
        @can('product-list')
            <div class="col-4">
                <div class="card">
                    <div class="card-body bg-success rounded text-white">
                        <h3>
                            Products
                        </h3>
                        <h1 class="text-end">
                            {{ $product_count }}
                        </h1>
                        <div class="text-center">
                            <a class="nav-link text-white" href="{{ route('product.index') }}">
                                More Info
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        @endcan
        <div class="col-4">
            <div class="card">
                <div class="card-body bg-primary rounded text-white">
                    <h3>
                        Users
                    </h3>
                    <h1 class="text-end">
                        {{ $user_count }}
                    </h1>
                    <div class="text-center">
                        <a class="nav-link text-white" href="{{ route('home') }}">
                            More Info
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
